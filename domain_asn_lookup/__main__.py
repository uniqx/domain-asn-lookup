#! /usr/bin/env python3

# SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: AGPL-3.0-or-later

from . import main

if __name__ == "__main__":
    main.main()
