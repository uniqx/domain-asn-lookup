#! /usr/bin/env python3

# SPDX-FileCopyrightText: 2020 Michael Pöhn <michael@poehn.at>
# SPDX-License-Identifier: CC0-1.0

import domain_asn_lookup
import setuptools


with open('README.md', 'r', encoding="utf-8") as f:
    long_description = f.read()

with open('requirements.txt', 'r', encoding="utf-8") as f:
    requirements_list = [x.strip() for x in f.readlines() if x.strip() and not x.strip().startswith('#')]


d = {'name': "domain-asn-lookup",
     'version': domain_asn_lookup.VERSION,
     'author': 'unique entity',
     'author_email': 'uniq@h4x.at',
     'description': 'A simple tool for resolving domain names to asn numbers.',
     'long_description': long_description,
     'long_description_content_type': 'text/markdown',
     'url': 'https://gitlab.com/uniqx/domain-asn-lookup',
     'license': 'AGPL-3.0-or-later',
     'packages': ['domain_asn_lookup'],
     'scripts': ['domain-asn-lookup'],
     'python_requires': '>=3.4',
     'classifiers': ['Programming Language :: Python :: 3',
                     'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
                     'Operating System :: OS Independent'],
     'install_requires': requirements_list}


setuptools.setup(**d)
