<!--
SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
SPDX-License-Identifier: CC0-1.0
-->

# Domain ASN lookup

Rudimentary tool for looking up ASNs for domain names.

NOTE: this script will automatically download/update a compressed ASN database
(~100MB) and store it uncompressed in your local cache folder (~2GB). So
startup times may varry.

## useage examples

### simple install

```
python3 -m pip install --user git+https://gitlab.com/uniqx/domain-asn-lookup.git@tags/v0.1.1
```

### lookup all ASNs for all domains in AdAway block list

```
curl -s 'https://raw.githubusercontent.com/AdAway/adaway.github.io/master/hosts.txt' | tac | grep '^127' | awk '{print $2}' | python3 -m domain_asn_lookup
```

### lookup all ASNs for all domains in notracking hosts-blocklist

```
curl -s  https://raw.githubusercontent.com/notracking/hosts-blocklists/master/domains.txt | tac | grep '^address=.*$' | sed 's@address=/\(.*\)/[0\.:]*@\1@' | uniq | python3 -m domain_asn_lookup
```

### dev setup

```
git clone https://gitlab.com/uniqx/domain_asn_lookup.git
cd domain_asn_lookup
python3 -m venv env
. env/bin/activate
python3 -m pip install -r requirements.txt
echo "example.com" | python3 -m domain_asn_lookup
```
